clear
close all
clc

load('DATA_Fig3.mat')

DATA.Human.MASKS.OMPFC.cdata=DATA.Human.MASKS.OMPFC.data{1,1}.data;
DATA.Human.MASKS.FCA_cluster.cdata=DATA.Human.MASKS.FCA_cluster.data{1,1}.data;
DATA.Human.MASKS.FA_cluster.cdata=DATA.Human.MASKS.FA_cluster.data{1,1}.data;
DATA.Human.MASKS.RRA_lateral_cluster.cdata=DATA.Human.MASKS.RRA_lateral_cluster.data{1,1}.data;

DATA.Macaque.MASKS.OMPFC.cdata=DATA.Macaque.MASKS.OMPFC.data{1,1}.data;
DATA.Macaque.MASKS.FCA_cluster.cdata=DATA.Macaque.MASKS.FCA_cluster.data{1,1}.data;
DATA.Macaque.MASKS.RRA_lateral_cluster.cdata=DATA.Macaque.MASKS.RRA_lateral_cluster.data{1,1}.data;
DATA.Macaque.MASKS.RRA_medial_cluster.cdata=DATA.Macaque.MASKS.RRA_medial_cluster.data{1,1}.data;

%% HUMAN

list_subs=fieldnames(DATA.Human.Reward_asymmetry);


id_ompfc=find(DATA.Human.MASKS.OMPFC.cdata==1);
id_clustFCA=find((DATA.Human.MASKS.FCA_cluster.cdata(id_ompfc)));
id_clust_FA=find((DATA.Human.MASKS.FA_cluster.cdata(id_ompfc)==3));

for ss = 1:length(list_subs)
    
    contrast_sub_REWARDFCA(ss,:) = DATA.Human.Reward_asymmetry.(list_subs{ss}).unsigned.OMPFC(id_clustFCA);
    contrast_sub_REWARDFA(ss,:)  = DATA.Human.Reward_asymmetry.(list_subs{ss}).signed.OMPFC(id_clust_FA);
    
    contrast_sub_FCAinRRA(ss,:) = nanmean(DATA.Human.Connectivity_asymmetry.(list_subs{ss}).WholeBrain(DATA.Human.MASKS.RRA_lateral_cluster.cdata == 1));
    contrast_sub_FCAinFA(ss,:)  = nanmean(DATA.Human.Connectivity_asymmetry.(list_subs{ss}).WholeBrain(DATA.Human.MASKS.FA_cluster.cdata == 3));
    contrast_sub_FCA_in_FCA(ss,:) = nanmean(DATA.Human.Connectivity_asymmetry.(list_subs{ss}).WholeBrain(DATA.Human.MASKS.FCA_cluster.cdata == 1));
    
end

RRA        = nanmean((contrast_sub_REWARDFCA)');
FCA        = (contrast_sub_FCAinRRA);
RRA_common = nanmean(contrast_sub_REWARDFA');

FCA_common = (contrast_sub_FCAinFA);
disp('FCA in FCA cluster:')

ttest_displayed(nanmean(contrast_sub_FCA_in_FCA,2));


RRA_yellow  = [.9 .9 0];
FCA_green   = [0 0.5 0];
FCA_rra_red = [.7 0 0];

figure
subplot(2,3,1)
b = barplot(nanmean(FCA),nanstd(FCA)/sqrt(57),FCA_green);
b.EdgeColor = RRA_yellow;
b.LineWidth = 2;
set(gca,'Xtick',1,'XtickLabel','RRA cluster','FontSize',14)
ylabel('FCA')
disp('FCA in RRA cluster:')
box off
ylim([0 1.2])
xlim([0 2])

ttest_displayed(FCA);

subplot(2,3,2)
b = barplot(nanmean(RRA),nanstd(RRA)/sqrt(57),RRA_yellow);
b.EdgeColor = FCA_green;
b.LineWidth = 2;
set(gca,'Xtick',1,'XtickLabel','FCA cluster','FontSize',14)
ylabel('RRA')
box off
ylim([0 1.2])
xlim([0 2])

disp('RRA in FCA cluster:')

ttest_displayed(RRA);

subplot(2,3,3)
s = scatter(RRA_common,FCA_common,70,'filled','MarkerFaceColor',[.7 0 0],'MarkerEdgeColor','k');
set(gca,'FontSize',14)
ylabel('FCA')
xlabel('RRA')
betas = glmfit(RRA_common,FCA_common);
fit   = glmval(betas,[-4 7],'identity');
hold on
plot([-4 7],fit,'color',FCA_rra_red)
box off
xlim([-2 3])
ylim([-1 4])
[r,p] = corrcoef(RRA_common,FCA_common);
disp(['Correlation between FCA and RRA: r=' num2str(r(2)) ', p=' num2str(p(2))]);


%% MACAQUE

% Indexes for OMPFC, FA Cluster and RRA cluster
id_ompfc=find(DATA.Macaque.MASKS.OMPFC.cdata==1);
id_clust_FCA=find((DATA.Macaque.MASKS.FCA_cluster.cdata(id_ompfc)+DATA.Macaque.MASKS.RRA_lateral_cluster.cdata(id_ompfc)) ==3);

id_clust_RRA_lateral=find((DATA.Macaque.MASKS.RRA_lateral_cluster.cdata(id_ompfc)));
id_clust_RRA_medial=find((DATA.Macaque.MASKS.RRA_medial_cluster.cdata(id_ompfc)));


for m=1:length(DATA.Macaque.MonkeyInfos.NamesReward)
    
    DATA.Macaque.Reward_asymmetry.(['Macaque_' num2str(m)]).signed.Tsat_sessionWholeBrain.cdata=...
        DATA.Macaque.Reward_asymmetry.(['Macaque_' num2str(m)]).signed.Tsat_sessionWholeBrain.data{1,1}.data;
    DATA.Macaque.Reward_asymmetry.(['Macaque_' num2str(m)]).unsigned.Tsat_sessionWholeBrain.cdata=...
        DATA.Macaque.Reward_asymmetry.(['Macaque_' num2str(m)]).unsigned.Tsat_sessionWholeBrain.data{1,1}.data;
    
    
    % GET signed RRA for FA CLUSTER
    reward_signed=DATA.Macaque.Reward_asymmetry.(['Macaque_' num2str(m)]).signed.Tsat_sessionWholeBrain;
    stockR_FA(m,:)=nanmean(((reward_signed.cdata(DATA.Macaque.MASKS.FCA_cluster.cdata==1 & DATA.Macaque.MASKS.RRA_lateral_cluster.cdata~=0))));
    
    % GET unsigned RRA for FCA CLUSTER
    
    reward_unsigned=DATA.Macaque.Reward_asymmetry.(['Macaque_' num2str(m)]).unsigned.Tsat_sessionWholeBrain;
    stockR_FCA(m,:)=nanmean(((reward_unsigned.cdata(DATA.Macaque.MASKS.FCA_cluster.cdata==1))));
    
    
    % Identify monkey id for loading FCA
    id_m=find(strcmp(DATA.Macaque.MonkeyInfos.NamesReward{m},DATA.Macaque.MonkeyInfos.NamesConn));
    monkey_id(m)=id_m;
    
    stockC_inFCA(m,:)=((nanmean(DATA.Macaque.Connectivity_asymmetry.signed.Tsat_sessionOMPFC(id_m,find(id_clust_FCA)),2)));
    
end

% GET Connectivity asym
stockC_inRRAlateral=((nanmean(DATA.Macaque.Connectivity_asymmetry.signed.Tsat_sessionOMPFC(:,(id_clust_RRA_lateral)),2)));
stockC_inRRAmedial=((nanmean(DATA.Macaque.Connectivity_asymmetry.signed.Tsat_sessionOMPFC(:,(id_clust_RRA_medial)),2)));


% combine reward and connectivity data per macaque
[monkeys,a]=unique(monkey_id);

monkey_RRA=NaN(length(monkeys),6);
for i=1:length(monkeys)
    monkey_pointsRFA_COMPFC(i,:)=[mean(stockR_FA(monkey_id==monkeys(i))) mean(stockC_inFCA(monkey_id==monkeys(i)))];
    monkey_RRA(i,1:sum(monkey_id==monkeys(i)))=stockR_FA(monkey_id==monkeys(i));
    
end


%% STATISTICS AND FIGURE 3 (bottom - macaque)

disp('FCA in RRA lateral cluster:')
% ttest_displayed(((nanmean(DATA.Connectivity_asymmetry.(['Macaque_' num2str(m)]).signed.Tsat_sessionOMPFC(:,id_clust_RRA_lateral),2))));
ttest_displayed(stockC_inRRAlateral);

disp('FCA in RRA medial cluster:')
ttest_displayed(stockC_inRRAmedial);

disp('RRA in FCA cluster:')
ttest_displayed(stockR_FCA);

stat_group=stockC_inRRAlateral;
stat_group2=stockC_inRRAmedial;


disp('Correlation across sessions')
[r,p]=corrcoef(stockR_FA,stockC_inFCA);
disp(['r=' num2str(r(2)) ', p=' num2str(p(2))]);

disp('Correlation across monkeys')
[r,p]=corrcoef(monkey_pointsRFA_COMPFC(:,1),monkey_pointsRFA_COMPFC(:,2));
disp(['r=' num2str(r(2)) ', p=' num2str(p(2))]);


% Figure
RRA_yellow  = [.9 .9 0];
FCA_green   = [0 0.5 0];
FCA_rra_red = [.7 0 0];


subplot(2,3,4)

b=barplot([nanmean(nanmean(stat_group,2)) nanmean(nanmean(stat_group2,2))],[nanstd(nanmean(stat_group,2))/sqrt(14) nanstd(nanmean(stat_group2,2))/sqrt(14)],FCA_green);
b(1).EdgeColor = RRA_yellow;
b(1).LineWidth = 2;
b(2).EdgeColor = RRA_yellow;
b(2).LineWidth = 2;
set(gca,'Xtick',1:2,'XtickLabel',{'lateral','medial'},'FontSize',14)
xlabel('RRA cluster')
ylabel('FCA')
box off
ylim([-.45 1])

subplot(2,3,5)

b = barplot(nanmean(stockR_FCA),nanstd(stockR_FCA)/sqrt(length(stockR_FCA)),RRA_yellow);
b.EdgeColor = FCA_green;
b.LineWidth = 2;
set(gca,'Xtick',1,'XtickLabel','FCA cluster','FontSize',14)
ylabel('RRA')
box off
ylim([-.45 1])
xlim([0 2])



subplot(2,3,6)

hold on
for m=1:length(monkeys)
    sem(m)=nanstd(monkey_RRA(m,~isnan(monkey_RRA(m,:))))/sqrt(sum(~isnan(monkey_RRA(m,:))));
end
herrorbar(monkey_pointsRFA_COMPFC(:,1)',monkey_pointsRFA_COMPFC(:,2)',sem,'.r')
hold on
scatter(monkey_pointsRFA_COMPFC(:,1)',monkey_pointsRFA_COMPFC(:,2)',200,'filled','MarkerFaceColor',FCA_rra_red,'MarkerEdgeColor','k');
hold on
scatter(stockR_FA,stockC_inFCA,30,'filled','MarkerFaceColor','k','MarkerEdgeColor','k')
set(gca,'FontSize',14)
xlabel('RRA')
ylabel('FCA')

l=lsline;
l(1).LineStyle='--';
l(1).Color=FCA_rra_red;
l(3).LineStyle='--';
l(3).Color=FCA_rra_red;
l(2).LineStyle='--';
l(2).Color='k';
