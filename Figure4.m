%% Script to get statistics on the fingerprint of FCA seed with DMN and ExN
% This script compute the connectivity of each seed with each network for
% each monkey and then do an ANOVA with repeated measures on the data. 

clear
close all
clc

load('DATA_Fig4.mat')

%% MACAQUE

data=[mean([DATA.Macaque.FALeft_Connectivity.DMN.Left.perSubArea DATA.Macaque.FALeft_Connectivity.DMN.Right.perSubArea...
            DATA.Macaque.FALeft_Connectivity.ExN.Right.perSubArea DATA.Macaque.FALeft_Connectivity.ExN.Left.perSubArea]);...
      mean([DATA.Macaque.FARight_Connectivity.DMN.Left.perSubArea DATA.Macaque.FARight_Connectivity.DMN.Right.perSubArea...
            DATA.Macaque.FARight_Connectivity.ExN.Right.perSubArea DATA.Macaque.FARight_Connectivity.ExN.Left.perSubArea])]';

for rD=1:size(DATA.Macaque.FALeft_Connectivity.DMN.Left.perSubArea,2)
    names{rD} = ['DMNL' num2str(rD)];
    names{rD} = [' '];   
end
for rDR=1:size(DATA.Macaque.FALeft_Connectivity.DMN.Right.perSubArea,2)
    names{rDR+rD} = ['DMNR' num2str(rDR)];
    names{rDR+rD} = [' '];   
end
for rE=1:size(DATA.Macaque.FALeft_Connectivity.ExN.Left.perSubArea,2)
    names{rD+rDR+rE} = ['ExNL' num2str(rE)];
    names{rD+rDR+rE} = [' ' ];   
end
for rER=1:size( DATA.Macaque.FALeft_Connectivity.ExN.Right.perSubArea,2)
    names{rD+rDR+rE+rER} = ['ExNL' num2str(rER)];
    names{rD+rDR+rE+rER} = [' '];
end

figure(1)
axs=subplot(4,3,[7 8 10 11])
[hf, ha, hp] = spider_wedge((data),'PlotType','patch','WedgeWidth',1,'Label',names,'NrTickMarks',2,'Range',[-.25 .25],'Title','','Handle', axs);


hp{2}.EdgeColor=[0.9 .4 .1];
hp{1}.EdgeColor=[0 .5 .9];

%% STATISTICS
Y=[];
factor_roi=[];
factor_network=[];
factor_hemis=[];
factor_sub=[];
for sid=1:2
    for N=1:2
        for sid2=1:2
            
Y=[Y; DATA.Macaque.Full_network_FA_connect.data(:,sid,N,sid2)];
factor_roi=[factor_roi ; sid*ones(14,1)];
factor_network=[factor_network ; N*ones(14,1)];
factor_hemis=[factor_hemis; sid2*ones(14,1)];
factor_sub=[factor_sub;[1:14]'];


        end
    end
end

% ANOVA WITH SUBJ AS RANDOM FACTOR
[p,tbl,stats]=anovan(Y,[factor_roi factor_network factor_hemis factor_sub],'model','full','random',4,'varnames',{'SeedSide','Network','Network Side', 'subjs'},'display','off');

% ANOVA FOR MULTIPLE COMPARISON (POST-HOC TESTS)

[p,tbl,stats]=anovan(Y,[factor_roi factor_network factor_hemis],'model','full','varnames',{'SeedSide','Network','Network Side'},'display','off');

% figure
% multcompare(stats,'dimension',[1 2])

factor_network_side=factor_hemis;

col_FA_left = [0 .5 1];
col_FA_right = [1 .3 0];


figure(1)
axs=subplot(4,3,[9 12])


data1 = mean([Y(factor_roi==1 & factor_network==1 & factor_network_side==1) Y(factor_roi==1 & factor_network==1 & factor_network_side==2)],2);
data2 = mean([Y(factor_roi==2 & factor_network==1 & factor_network_side==1) Y(factor_roi==2 & factor_network==1 & factor_network_side==2)],2);

data3 = mean([Y(factor_roi==1 & factor_network==2 & factor_network_side==1) Y(factor_roi==1 & factor_network==2 & factor_network_side==2)],2);
data4 = mean([Y(factor_roi==2 & factor_network==2 & factor_network_side==1) Y(factor_roi==2 & factor_network==2 & factor_network_side==2)],2);


b=barplot(mean([data2-data1 data4-data3]),...
    [std([data1-data2 data4-data3])]/sqrt(14),[ 0 .7 0;.9 0 0]);
set(gca,'Xtick',1:2,'XtickLabel',{'DMN', 'ExN'},'FontSize',14)
ylabel('Right - Left FA connectivity')


for bb=1:2
    b(bb).BarWidth = 0.8;
end
ylim([-.03 .07])



data1 = mean([Y(factor_roi==1 & factor_network==1 & factor_network_side==1) Y(factor_roi==1 & factor_network==1 & factor_network_side==2)],2);
data2 = mean([Y(factor_roi==2 & factor_network==1 & factor_network_side==1) Y(factor_roi==2 & factor_network==1 & factor_network_side==2)],2);
disp('Left seed with DMN:')
disp(['mean: ' num2str(mean(data1)) ', SEM: ' num2str(std(data1)/sqrt(14))])
ttest_displayed(data1);

disp('right seed with DMN:')
disp(['mean: ' num2str(mean(data2)) ', SEM: ' num2str(std(data2)/sqrt(14))])
ttest_displayed(data2);


%% HUMAN
% 
% 
% DATA.Human.FALeft_Connectivity.DMN.Left.perSubArea=squeeze(stock_sub{1}(:,1,1:10));
% DATA.Human.FALeft_Connectivity.DMN.Right.perSubArea=squeeze(stock_sub{1}(:,1,11:17));
% DATA.Human.FARight_Connectivity.DMN.Left.perSubArea=squeeze(stock_sub{1}(:,2,1:10));
% DATA.Human.FARight_Connectivity.DMN.Right.perSubArea=squeeze(stock_sub{1}(:,2,11:17));
% 
% DATA.Human.FALeft_Connectivity.ExN.Left.perSubArea=squeeze(stock_sub{2}(:,1,8:13));
% DATA.Human.FALeft_Connectivity.ExN.Right.perSubArea=squeeze(stock_sub{2}(:,1,1:7));
% DATA.Human.FARight_Connectivity.ExN.Left.perSubArea=squeeze(stock_sub{2}(:,2,8:13));
% DATA.Human.FARight_Connectivity.ExN.Right.perSubArea=squeeze(stock_sub{2}(:,2,1:7));


data=[mean([DATA.Human.FALeft_Connectivity.DMN.Left.perSubArea DATA.Human.FALeft_Connectivity.DMN.Right.perSubArea...
            DATA.Human.FALeft_Connectivity.ExN.Right.perSubArea DATA.Human.FALeft_Connectivity.ExN.Left.perSubArea]);...
      mean([DATA.Human.FARight_Connectivity.DMN.Left.perSubArea DATA.Human.FARight_Connectivity.DMN.Right.perSubArea...
            DATA.Human.FARight_Connectivity.ExN.Right.perSubArea DATA.Human.FARight_Connectivity.ExN.Left.perSubArea])]';

%% FINGERPRINTS

for rD=1:17
    names{rD} = ['DMN' num2str(rD)];
    names{rD} = [' '];   
end
for rE=1:13
    names{rD+rE} = ['ExN' num2str(rE)];
    names{rD+rE} = [' ' ];   
end

axs=subplot(4,3,[1 2 4 5])

[hf, ha, hp] = spider_wedge(data([11:17 1:10 18:30],:),'PlotType','patch','WedgeWidth',1,'Label',names,'NrTickMarks',2,'Range',[-.1 .1],'Handle',axs,'Title','');

hp{2}.EdgeColor=[0.9 .4 .1];
hp{1}.EdgeColor=[0 .5 .9];


%% STATISTICS


Y  = [nanmean(DATA.Human.FALeft_Connectivity.DMN.Left.perSubArea,2);...
    nanmean(DATA.Human.FALeft_Connectivity.DMN.Right.perSubArea,2);...
    nanmean(DATA.Human.FALeft_Connectivity.ExN.Right.perSubArea,2);...
    nanmean(DATA.Human.FALeft_Connectivity.ExN.Left.perSubArea,2);...
      nanmean(DATA.Human.FARight_Connectivity.DMN.Left.perSubArea,2);...
      nanmean(DATA.Human.FARight_Connectivity.DMN.Right.perSubArea,2);...
      nanmean(DATA.Human.FARight_Connectivity.ExN.Right.perSubArea,2);...
      nanmean(DATA.Human.FARight_Connectivity.ExN.Left.perSubArea,2)];
  
factor_roi          = [ones(57*4,1); 2*ones(57*4,1)];
factor_network      = [ones(57*2,1); 2*ones(57*2,1); ones(57*2,1); 2*ones(57*2,1)];
factor_network_side = [ones(57*1,1); 2*ones(57*1,1); ones(57*1,1); 2*ones(57*1,1);ones(57*1,1); 2*ones(57*1,1); ones(57*1,1); 2*ones(57*1,1)];
sub_factor          = repmat([1:57]',8,1);

% ANOVA WITH SUBJ AS RANDOM FACTOR
[p,tbl,stats]=anovan(Y,[factor_roi factor_network factor_network_side sub_factor],'model','full','random',4,'varnames',{'SeedSide','Network','Network Side', 'subjs'},'display','off');




subplot(4,3,[3 6])

data1 = mean([Y(factor_roi==1 & factor_network==1 & factor_network_side==1) Y(factor_roi==1 & factor_network==1 & factor_network_side==2)],2);
data2 = mean([Y(factor_roi==2 & factor_network==1 & factor_network_side==1) Y(factor_roi==2 & factor_network==1 & factor_network_side==2)],2);

data3 = mean([Y(factor_roi==1 & factor_network==2 & factor_network_side==1) Y(factor_roi==1 & factor_network==2 & factor_network_side==2)],2);
data4 = mean([Y(factor_roi==2 & factor_network==2 & factor_network_side==1) Y(factor_roi==2 & factor_network==2 & factor_network_side==2)],2);


b=barplot(mean([data2-data1 data4-data3]),...
    [std([data1-data2 data4-data3])]/sqrt(57),[ 0 .7 0;.9 0 0])
set(gca,'Xtick',1:2,'XtickLabel',{'DMN', 'ExN'},'FontSize',14)
ylabel('Right - Left FA connectivity')


for bb=1:2
    b(bb).BarWidth = 0.8;
end


ttest_displayed(data2-data1);




data1 = mean([Y(factor_roi==1 & factor_network==1 & factor_network_side==1) Y(factor_roi==1 & factor_network==1 & factor_network_side==2)],2);
data2 = mean([Y(factor_roi==2 & factor_network==1 & factor_network_side==1) Y(factor_roi==2 & factor_network==1 & factor_network_side==2)],2);
disp('Left seed with DMN:')
disp(['mean: ' num2str(mean(data1)) ', SEM: ' num2str(std(data1)/sqrt(57))])
ttest_displayed(data1);

disp('right seed with DMN:')
disp(['mean: ' num2str(mean(data2)) ', SEM: ' num2str(std(data2)/sqrt(57))])
ttest_displayed(data2);