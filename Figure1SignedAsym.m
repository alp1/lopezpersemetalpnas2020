clear
close all
clc


load('DATA_Fig1S1.mat')

%% HUMAN

DATA.Human.Mask.RRAcluster.cdata=DATA.Human.Mask.RRAcluster.data{1,1}.data;
DATA.Macaque.Mask.RRAcluster.cdata=DATA.Macaque.Mask.RRAcluster.data{1,1}.data;

idx_cluster = find(DATA.Human.Mask.RRAcluster.cdata==1);


diff = DATA.Human.Reward_in_leftHemisphere-DATA.Human.Reward_in_rightHemisphere;
diff_zs = zscore(diff')';

diff_to_plot = mean(diff_zs(:,idx_cluster),2);

% FIGURE 1B
figure
subplot(2,4,[1 2 5 6])
histogram(-diff_to_plot,16,'FaceColor',[1 1 0],'EdgeColor',[0 0 0])
xlim([-2.3 2.3])
hold on
plot([mean(abs(diff_to_plot)) mean(abs(diff_to_plot))],[0 12],'--r');
ylim([0 12])

ttest_displayed(-diff_to_plot)


%% MONKEY

design=DATA.Macaque.Design;

diff = DATA.Macaque.Reward_in_leftHemisphere-DATA.Macaque.Reward_in_rightHemisphere;
diff_zs = zscore(diff')';

expe_point=(design(:,1)*100+design(:,2)*10+design(:,3));
list_expe_point =unique(expe_point);
for ep = 1:length(list_expe_point)
    expe_mapsL(ep,:) = mean(DATA.Macaque.Reward_in_leftHemisphere(expe_point==list_expe_point(ep),:));
    expe_mapsR(ep,:) = mean(DATA.Macaque.Reward_in_rightHemisphere(expe_point==list_expe_point(ep),:));
    diff_ep_zs(ep,:) = mean(diff_zs(expe_point==list_expe_point(ep),:));
end

diff_to_plot= mean(diff_ep_zs(:,DATA.Macaque.Mask.RRAcluster.cdata==1),2);

% FIGURE 1D
subplot(2,4,[3 4])

histogram(-diff_to_plot,8,'FaceColor',[1 1 0],'EdgeColor',[0 0 0])
xlabel('Right-Left')
xlim([-2.3 2.3])
hold on
plot([mean(abs(diff_to_plot)) mean(abs(diff_to_plot))],[0 12],'--r');
hold on
ylim([0 12])

ttest_displayed((-diff_to_plot))

%% LATERAL

diff_to_plot= mean(diff_ep_zs(:,DATA.Macaque.Mask.RRAcluster.cdata==2),2);

subplot(2,4,[7 8])
histogram(-diff_to_plot,8,'FaceColor',[1 1 0],'EdgeColor',[0 0 0])
hold on
plot([mean(abs(diff_to_plot)) mean(abs(diff_to_plot))],[0 12],'--r');
hold on
xlim([-2.3 2.3])
ylim([0 12])

ttest_displayed((-diff_to_plot));


expe_point2=(design(:,1)*100);
expe_point2(expe_point2==300)=200;
list_expe_point2 =unique(expe_point2);

% FIGURE S1 Medial
subplots=[2 4 6];
clust = 1;
figure (2)
prot_names={'1','2 & 3', '4'};
for p=1:length(list_expe_point2)
    subplot(round(length(list_expe_point2)),2,subplots(p))
    histogram(mean(-diff_zs(expe_point2==list_expe_point2(p),DATA.Macaque.Mask.RRAcluster.cdata==clust),2),16,'FaceColor',[1 1 0],'EdgeColor',[0 0 0])
    [~,pval,~,stat]=ttest(mean(-diff_zs(expe_point2==list_expe_point2(p),DATA.Macaque.Mask.RRAcluster.cdata==clust),2));
    title(['P' prot_names{p} ': t(' num2str(stat.df) ')=' num2str(round(stat.tstat*1000)/1000) ', p=' num2str(round(pval*1000)/1000)])
    xlim([-2.3 2.3])
    hold on
    plot(-[0 0],[0 20],'--r');
    xlabel('Right-Left')
    ttest_displayed(mean(-diff_zs(expe_point2==list_expe_point2(p),DATA.Macaque.Mask.RRAcluster.cdata==clust),2));
    ylabel('Sessions')
end

% FIGURE S1 Lateral
subplots=[1 3 5];

clust = 2;
figure (2)
prot_names={'1','2 & 3', '4'};
for p=1:length(list_expe_point2)
    subplot(round(length(list_expe_point2)),2,subplots(p))
    histogram(mean(-diff_zs(expe_point2==list_expe_point2(p),DATA.Macaque.Mask.RRAcluster.cdata==clust),2),16,'FaceColor',[1 1 0],'EdgeColor',[0 0 0])
    [~,pval,~,stat]=ttest(mean(-diff_zs(expe_point2==list_expe_point2(p),DATA.Macaque.Mask.RRAcluster.cdata==clust),2));
    title(['P' prot_names{p} ': t(' num2str(stat.df) ')=' num2str(round(stat.tstat*1000)/1000) ', p=' num2str(round(pval*1000)/1000)])
    xlim([-2.3 2.3])
    hold on
    plot(-[0 0],[0 20],'--r');
    xlabel('Right-Left')
    ttest_displayed(mean(-diff_zs(expe_point2==list_expe_point2(p),DATA.Macaque.Mask.RRAcluster.cdata==clust),2));
    ylabel('Sessions')
end